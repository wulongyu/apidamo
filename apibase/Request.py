#coding:utf-8
import hashlib
import time
import json
import requests

from apibase.Signature import SignatureVerification

def Request_T(url,data,Token):

    # 去除字典中value为空的key
    for key in list(data.keys()):
        if not data.get(key):
            del data[key]

    Signature1= SignatureVerification(data,Token)

    # print("Request_T_Signature:",Signature1)

    # data1=data.update({"Signature":Signature1})
    #添加键值对Signature
    data['Signature']=Signature1

    headers = {'Content-Type': 'application/json;charset=utf-8'}

    # print("请求参数:",data)

    res = requests.post(url, data=json.dumps(data), headers=headers)

    # print("Request_T响应数据：",res)

    # print("Requests响应Type：",type(res))

    print('响应时间：',res.elapsed)
    r = json.loads(res.text)

    # print(r)

    return r
    #返回data.text
