import base64
import hashlib
import re

# 签名验证方法
def SignatureVerification(InterfaceDictionary, token):
    keys = sorted(InterfaceDictionary, key=str.lower)
    ab = []
    for key in keys:
        value = InterfaceDictionary.get(key)
        # value2 = "".join(value1)
        '''将list转换为str'''
        # value=str(value2)
        ab.append(key + "=" + value)
    if token != "":
        ab.append("key=" + token)
    str1 = ("&".join(ab))
    print('参加签名数据：',str1)
    hl = hashlib.md5()
    hl.update(str1.encode(encoding='utf-8'))
    return hl.hexdigest().upper()


body = {
    "Nonce": "q2j2ah1ch1urv9ksgqa0",
    "ReqTime": "1568182132869",
    "TerminalType": "Android",
    "AppID": "1004",
    "Phone": "16621335133",
    "TerminalVersion": "1.3.0",
    "Password": "v7SsZeXlWH8="
}

body1 = {
    "AccountID": "3261",
    "AppID": "3",
    "Data": '[{"ArrivePayAmount": "0","ExpressCompanyId": "2","ExpressNo": "20190829001","ReceiverMobile": "18860279291","ShelfNo": "20190829"}]',
    "Nonce": "4h8p0dpeid9c0eal72dd",
    "ReqTime": "1568107157493",
    "StationId": '88687834',
    "TenantID": "88687834",
    "TerminalType": "Android",
    "TerminalVersion": "1.0.0t12"
}

body0924 = {
    "DeviceId": "3480",
    "Time": "3",
    "OversizeGridFee": "0.05",
    "StationName": "漕河泾京东快递柜",
    "TenantID": "88687814",
    "ReqTime": "1569325103538",
    "AppID": "1004",
    "LargeGridFee": "0.04",
    "DeviceGroupId": "462",
    "LargeGrid": "1",
    "AccountID": "3489",
    "SmallGrid": "0",
    "Amount": "0.04",
    "StationArea": "上海上海市徐汇区田林路398号",
    "LeaseMode": "1",
    "StationId": "88688226",
    "TerminalVersion": "1.3.1t5",
    "Nonce": "v3va1x5kn4pxgyjrzisa",
    "TerminalType": "Android",
    "DeviceGroupSN": "662487125461",
    "MediumGrid": "0",
    "SmallGridFee": "0.02",
    "OversizeGrid": "0",
    "MediumGridFee": "0.03"
}